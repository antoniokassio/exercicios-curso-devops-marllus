# ATV1.Executando seu Primeiro Container
- OutPut
``` language:bash
docker run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
719385e32844: Pull complete 
Digest: sha256:dcba6daec718f547568c562956fa47e1b03673dd010fe6ee58ca806767031d1c
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
# ATV02. Construindo e Executando uma Imagem Personalizada
- OutPut
``` language:bash
[node1] (local) root@192.168.0.13 ~
$ mkdir minha-imagem
[node1] (local) root@192.168.0.13 ~
$ cd minha-imagem
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ 
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ vim Dockfile
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker build -t minha-imagem .
[+] Building 0.1s (2/2) FINISHED                                                                                
 => [internal] load .dockerignore                                                                          0.0s
 => => transferring context: 2B                                                                            0.0s
 => [internal] load build definition from Dockerfile                                                       0.0s
 => => transferring dockerfile: 2B                                                                         0.0s
ERROR: failed to solve: failed to read dockerfile: open /var/lib/docker/tmp/buildkit-mount2693288167/Dockerfile: no such file or directory
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ ls 0la
ls: 0la: No such file or directory
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ ls -la
total 4
drwxr-xr-x    2 root     root            22 Sep  3 22:07 .
drwx------    1 root     root            57 Sep  3 22:08 ..
-rw-r--r--    1 root     root           100 Sep  3 22:07 Dockfile
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mv Dockfile Dockerfile
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker build -t minha-imagem .
[+] Building 12.1s (6/6) FINISHED                                                                               
 => [internal] load .dockerignore                                                                          0.0s
 => => transferring context: 2B                                                                            0.0s
 => [internal] load build definition from Dockerfile                                                       0.0s
 => => transferring dockerfile: 137B                                                                       0.0s
 => [internal] load metadata for docker.io/library/ubuntu:latest                                           0.3s
 => [1/2] FROM docker.io/library/ubuntu:latest@sha256:aabed3296a3d45cede1dc866a24476c4d7e093aa806263c27dd  2.6s
 => => resolve docker.io/library/ubuntu:latest@sha256:aabed3296a3d45cede1dc866a24476c4d7e093aa806263c27dd  0.0s
 => => sha256:445a6a12be2be54b4da18d7c77d4a41bc4746bc422f1f4325a60ff4fc7ea2e5d 29.54MB / 29.54MB           0.3s
 => => sha256:aabed3296a3d45cede1dc866a24476c4d7e093aa806263c27ddaadbdce3c1054 1.13kB / 1.13kB             0.0s
 => => sha256:b492494d8e0113c4ad3fe4528a4b5ff89faa5331f7d52c5c138196f69ce176a6 424B / 424B                 0.0s
 => => sha256:c6b84b685f35f1a5d63661f5d4aa662ad9b7ee4f4b8c394c022f25023c907b65 2.30kB / 2.30kB             0.0s
 => => extracting sha256:445a6a12be2be54b4da18d7c77d4a41bc4746bc422f1f4325a60ff4fc7ea2e5d                  2.0s
 => [2/2] RUN apt-get update && apt-get install -y curl                                                    8.7s
 => exporting to image                                                                                     0.4s
 => => exporting layers                                                                                    0.4s
 => => writing image sha256:47a5984859f89058ab44fde245356178336352bf4574f8839ff8b63f041c875b               0.0s
 => => naming to docker.io/library/minha-imagem                                                            0.0s
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker run minha-imagem
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    91  100    91    0     0    422      0 --:--:-- --:--:-- --:--:--   423
<html><body>You are being <a href="https://about.gitlab.com/">redirected</a>.</body></html>[node1] (local) root@192.168.0.13 ~/minha-imagem
```
# Montando um Volume
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mkdir dados-container
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker run -v $(pwd)/dados-container:/dados-container ubuntu:latest ls /dados-container
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
445a6a12be2b: Already exists 
Digest: sha256:aabed3296a3d45cede1dc866a24476c4d7e093aa806263c27ddaadbdce3c1054
Status: Downloaded newer image for ubuntu:latest
[node1] (local) root@192.168.0.13 ~/minha-imagem
```
# Redes de Contêineres
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker network create minha-rede
77fcfadb1966df98214f79204e1a8fc26e56d4e6f1366725c4efcf860017f9b4
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker run --network minha-rede --name cont1 -d nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
52d2b7f179e3: Pull complete 
fd9f026c6310: Pull complete 
055fa98b4363: Pull complete 
96576293dd29: Pull complete 
a7c4092be904: Pull complete 
e3b6889c8954: Pull complete 
da761d9a302b: Pull complete 
Digest: sha256:104c7c5c54f2685f0f46f3be607ce60da7085da3eaa5ad22d3d9f01594295e9c
Status: Downloaded newer image for nginx:latest
ce6b98bac6ee4689cd3adb09038decb809b740aa9692a9d057601ec035bbe63e
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker run --network minha-rede --name cont2 -d nginx
17a1b88a11fe324efdac1ec68c5628af565deb59664e1ea122dab9f527c9cd72
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker exec -it cont1 bash
root@ce6b98bac6ee:/# apt update
Get:1 http://deb.debian.org/debian bookworm InRelease [151 kB]
Get:2 http://deb.debian.org/debian bookworm-updates InRelease [52.1 kB]
Get:3 http://deb.debian.org/debian-security bookworm-security InRelease [48.0 kB]
Get:4 http://deb.debian.org/debian bookworm/main amd64 Packages [8906 kB]
Get:5 http://deb.debian.org/debian bookworm-updates/main amd64 Packages [4952 B]
Get:6 http://deb.debian.org/debian-security bookworm-security/main amd64 Packages [58.3 kB]
Fetched 9221 kB in 1s (6292 kB/s)                          
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
root@ce6b98bac6ee:/# apt install inetutils-ping
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  netbase
The following NEW packages will be installed:
  inetutils-ping netbase
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 98.7 kB of archives.
After this operation, 255 kB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://deb.debian.org/debian bookworm/main amd64 netbase all 6.4 [12.8 kB]
Get:2 http://deb.debian.org/debian bookworm/main amd64 inetutils-ping amd64 2:2.4-2 [85.9 kB]
Fetched 98.7 kB in 0s (393 kB/s)          
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package netbase.
(Reading database ... 7590 files and directories currently installed.)
Preparing to unpack .../archives/netbase_6.4_all.deb ...
Unpacking netbase (6.4) ...
Selecting previously unselected package inetutils-ping.
Preparing to unpack .../inetutils-ping_2%3a2.4-2_amd64.deb ...
Unpacking inetutils-ping (2:2.4-2) ...
Setting up netbase (6.4) ...
Setting up inetutils-ping (2:2.4-2) ...
root@ce6b98bac6ee:/#  ping cont2
PING cont2 (172.19.0.3): 56 data bytes
64 bytes from 172.19.0.3: icmp_seq=0 ttl=64 time=0.125 ms
64 bytes from 172.19.0.3: icmp_seq=1 ttl=64 time=0.112 ms
64 bytes from 172.19.0.3: icmp_seq=2 ttl=64 time=0.087 ms
64 bytes from 172.19.0.3: icmp_seq=3 ttl=64 time=0.101 ms
64 bytes from 172.19.0.3: icmp_seq=4 ttl=64 time=0.102 ms
^C--- cont2 ping statistics ---
5 packets transmitted, 5 packets received, 0% packet loss
round-trip min/avg/max/stddev = 0.087/0.105/0.125/0.000 ms
root@ce6b98bac6ee:/#
```
# Compondo com Docker Compose
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ vim docker-compose.yml
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ cat docker-compose.yml 
version: '3'
services:
  web:
    image: nginx
    ports:
    - "80:80"
  db:
    image: postgres
    environment:
      POSTGRES_PASSWORD: mysecretpassword
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ docker-compose up
[+] Running 14/14
 ✔ db 13 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                     7.1s 
   ✔ 52d2b7f179e3 Already exists                                                                           0.0s 
   ✔ d9c06b35c8a5 Pull complete                                                                            0.1s 
   ✔ ec0d4c36c7f4 Pull complete                                                                            0.4s 
   ✔ aa8e32a16a69 Pull complete                                                                            0.5s 
   ✔ 8950a67e90d4 Pull complete                                                                            1.2s 
   ✔ 1b47429b7c5f Pull complete                                                                            1.4s 
   ✔ a773f7da97bb Pull complete                                                                            1.4s 
   ✔ 7bddc9bbcf13 Pull complete                                                                            1.5s 
   ✔ 60829730fa39 Pull complete                                                                            6.6s 
   ✔ f3d9c845d2f3 Pull complete                                                                            6.6s 
   ✔ cfcd43fe346d Pull complete                                                                            6.7s 
   ✔ 576335d55cdb Pull complete                                                                            6.7s 
   ✔ caad4144446c Pull complete                                                                            6.7s 
[+] Building 0.0s (0/0)                                                                                         
[+] Running 3/3
 ✔ Network minha-imagem_default  Created                                                                   0.1s 
 ✔ Container minha-imagem-web-1  Created                                                                   0.1s 
 ✔ Container minha-imagem-db-1   Created                                                                   0.1s 
Attaching to minha-imagem-db-1, minha-imagem-web-1
minha-imagem-db-1   | The files belonging to this database system will be owned by user "postgres".
minha-imagem-db-1   | This user must also own the server process.
minha-imagem-db-1   | 
minha-imagem-db-1   | The database cluster will be initialized with locale "en_US.utf8".
minha-imagem-db-1   | The default database encoding has accordingly been set to "UTF8".
minha-imagem-db-1   | The default text search configuration will be set to "english".
minha-imagem-db-1   | 
minha-imagem-db-1   | Data page checksums are disabled.
minha-imagem-db-1   | 
minha-imagem-db-1   | fixing permissions on existing directory /var/lib/postgresql/data ... ok
minha-imagem-db-1   | creating subdirectories ... ok
minha-imagem-db-1   | selecting dynamic shared memory implementation ... posix
minha-imagem-web-1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
minha-imagem-web-1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
minha-imagem-web-1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
minha-imagem-web-1  | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
minha-imagem-db-1   | selecting default max_connections ... 100
minha-imagem-web-1  | 10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
minha-imagem-web-1  | /docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
minha-imagem-web-1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
minha-imagem-web-1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
minha-imagem-web-1  | /docker-entrypoint.sh: Configuration complete; ready for start up
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: using the "epoll" event method
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: nginx/1.25.2
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14) 
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: OS: Linux 4.4.0-210-generic
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker processes
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 28
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 29
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 30
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 31
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 32
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 33
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 34
minha-imagem-web-1  | 2023/09/03 22:31:53 [notice] 1#1: start worker process 35
minha-imagem-db-1   | selecting default shared_buffers ... 128MB
minha-imagem-db-1   | selecting default time zone ... Etc/UTC
minha-imagem-db-1   | creating configuration files ... ok
minha-imagem-db-1   | running bootstrap script ... ok
minha-imagem-db-1   | performing post-bootstrap initialization ... ok
minha-imagem-db-1   | syncing data to disk ... ok
minha-imagem-db-1   | 
minha-imagem-db-1   | 
minha-imagem-db-1   | Success. You can now start the database server using:
minha-imagem-db-1   | 
minha-imagem-db-1   |     pg_ctl -D /var/lib/postgresql/data -l logfile start
minha-imagem-db-1   | 
minha-imagem-db-1   | initdb: warning: enabling "trust" authentication for local connections
minha-imagem-db-1   | initdb: hint: You can change this by editing pg_hba.conf or using the option -A, or --auth-local and --auth-host, the next time you run initdb.
minha-imagem-db-1   | waiting for server to start....2023-09-03 22:31:54.452 UTC [48] LOG:  starting PostgreSQL 15.4 (Debian 15.4-1.pgdg120+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 12.2.0-14) 12.2.0, 64-bit
minha-imagem-db-1   | 2023-09-03 22:31:54.453 UTC [48] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
minha-imagem-db-1   | 2023-09-03 22:31:54.457 UTC [51] LOG:  database system was shut down at 2023-09-03 22:31:54 UTC
minha-imagem-db-1   | 2023-09-03 22:31:54.462 UTC [48] LOG:  database system is ready to accept connections
minha-imagem-db-1   |  done
minha-imagem-db-1   | server started
minha-imagem-db-1   | 
minha-imagem-db-1   | /usr/local/bin/docker-entrypoint.sh: ignoring /docker-entrypoint-initdb.d/*
minha-imagem-db-1   | 
minha-imagem-db-1   | 2023-09-03 22:31:54.581 UTC [48] LOG:  received fast shutdown request
minha-imagem-db-1   | waiting for server to shut down....2023-09-03 22:31:54.582 UTC [48] LOG:  aborting any active transactions
minha-imagem-db-1   | 2023-09-03 22:31:54.585 UTC [48] LOG:  background worker "logical replication launcher" (PID 54) exited with exit code 1
minha-imagem-db-1   | 2023-09-03 22:31:54.585 UTC [49] LOG:  shutting down
minha-imagem-db-1   | 2023-09-03 22:31:54.585 UTC [49] LOG:  checkpoint starting: shutdown immediate
minha-imagem-db-1   | 2023-09-03 22:31:54.590 UTC [49] LOG:  checkpoint complete: wrote 3 buffers (0.0%); 0 WAL file(s) added, 0 removed, 0 recycled; write=0.001 s, sync=0.002 s, total=0.006 s; sync files=2, longest=0.002 s, average=0.001 s; distance=0 kB, estimate=0 kB
minha-imagem-db-1   | 2023-09-03 22:31:54.594 UTC [48] LOG:  database system is shut down
minha-imagem-db-1   |  done
minha-imagem-db-1   | server stopped
minha-imagem-db-1   | 
minha-imagem-db-1   | PostgreSQL init process complete; ready for start up.
minha-imagem-db-1   | 
minha-imagem-db-1   | 2023-09-03 22:31:54.710 UTC [1] LOG:  starting PostgreSQL 15.4 (Debian 15.4-1.pgdg120+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 12.2.0-14) 12.2.0, 64-bit
minha-imagem-db-1   | 2023-09-03 22:31:54.711 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
minha-imagem-db-1   | 2023-09-03 22:31:54.712 UTC [1] LOG:  listening on IPv6 address "::", port 5432
minha-imagem-db-1   | 2023-09-03 22:31:54.714 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
minha-imagem-db-1   | 2023-09-03 22:31:54.717 UTC [62] LOG:  database system was shut down at 2023-09-03 22:31:54 UTC
minha-imagem-db-1   | 2023-09-03 22:31:54.721 UTC [1] LOG:  database system is ready to accept connections

[link do servidor criado](http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/)
Welcome to nginx!

If you see this page, the nginx web server is successfully installed and working. Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.
```
# Compondo com Docker Compose
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mkdir wordpress
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ cd wordpress/
[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
$ vim docker-compose.yml
[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
$ vim docker-compose.yml
[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
$ cat docker-compose.yml 
services:
  db:
    # We use a mariadb image which supports both amd64 & arm64 architecture
    image: mariadb:10.6.4-focal
    # If you really want to use MySQL, uncomment the following line
    #image: mysql:8.0.27
    command: '--default-authentication-plugin=mysql_native_password'
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=somewordpress
      - MYSQL_DATABASE=wordpress
      - MYSQL_USER=wordpress
      - MYSQL_PASSWORD=wordpress
    expose:
      - 3306
      - 33060
  wordpress:
    image: wordpress:latest
    ports:
      - 80:80
    restart: always
    environment:
      - WORDPRESS_DB_HOST=db
      - WORDPRESS_DB_USER=wordpress
      - WORDPRESS_DB_PASSWORD=wordpress
      - WORDPRESS_DB_NAME=wordpress
volumes:
  db_data:

[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
$ docker-compose up
[+] Running 33/12
 ✔ db 10 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                       10.3s 
 ✔ wordpress 21 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                     18.7s 
[+] Building 0.0s (0/0)                                                                                         
[+] Running 4/1
 ✔ Network wordpress_default        Created                                                                0.0s 
 ✔ Volume "wordpress_db_data"       Created                                                                0.0s 
 ✔ Container wordpress-db-1         Created                                                                0.1s 
 ✔ Container wordpress-wordpress-1  Created                                                                0.1s 
Attaching to wordpress-db-1, wordpress-wordpress-1
wordpress-db-1         | 2023-09-03 22:41:19+00:00 [Note] [Entrypoint]: Entrypoint script for MariaDB Server 1:10.6.4+maria~focal started.
wordpress-wordpress-1  | WordPress not found in /var/www/html - copying now...
wordpress-db-1         | 2023-09-03 22:41:20+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
wordpress-db-1         | 2023-09-03 22:41:20+00:00 [Note] [Entrypoint]: Entrypoint script for MariaDB Server 1:10.6.4+maria~focal started.
wordpress-db-1         | 2023-09-03 22:41:20+00:00 [Note] [Entrypoint]: Initializing database files
wordpress-wordpress-1  | Complete! WordPress has been successfully copied to /var/www/html
wordpress-wordpress-1  | No 'wp-config.php' found in /var/www/html, but 'WORDPRESS_...' variables supplied; copying 'wp-config-docker.php' (WORDPRESS_DB_HOST WORDPRESS_DB_NAME WORDPRESS_DB_PASSWORD WORDPRESS_DB_USER)
wordpress-db-1         | 2023-09-03 22:41:20 0 [Warning] 'default-authentication-plugin' is MySQL 5.6 / 5.7 compatible option. To be implemented in later versions.
wordpress-db-1         | 2023-09-03 22:41:20 0 [Warning] You need to use --log-bin to make --expire-logs-days or --binlog-expire-logs-seconds work.
wordpress-wordpress-1  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.21.0.3. Set the 'ServerName' directive globally to suppress this message
wordpress-wordpress-1  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.21.0.3. Set the 'ServerName' directive globally to suppress this message
wordpress-wordpress-1  | [Sun Sep 03 22:41:21.220714 2023] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.56 (Debian) PHP/8.0.30 configured -- resuming normal operations
wordpress-wordpress-1  | [Sun Sep 03 22:41:21.220792 2023] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
wordpress-db-1         | 
wordpress-db-1         | 
wordpress-db-1         | PLEASE REMEMBER TO SET A PASSWORD FOR THE MariaDB root USER !
wordpress-db-1         | To do so, start the server, then issue the following command:
wordpress-db-1         | 
wordpress-db-1         | '/usr/bin/mysql_secure_installation'
wordpress-db-1         | 
wordpress-db-1         | which will also give you the option of removing the test
wordpress-db-1         | databases and anonymous user created by default.  This is
wordpress-db-1         | strongly recommended for production servers.
wordpress-db-1         | 
wordpress-db-1         | See the MariaDB Knowledgebase at https://mariadb.com/kb or the
wordpress-db-1         | MySQL manual for more instructions.
wordpress-db-1         | 
wordpress-db-1         | Please report any problems at https://mariadb.org/jira
wordpress-db-1         | 
wordpress-db-1         | The latest information about MariaDB is available at https://mariadb.org/.
wordpress-db-1         | You can find additional information about the MySQL part at:
wordpress-db-1         | https://dev.mysql.com
wordpress-db-1         | Consider joining MariaDB's strong and vibrant community:
wordpress-db-1         | https://mariadb.org/get-involved/
wordpress-db-1         | 
wordpress-db-1         | 2023-09-03 22:41:21+00:00 [Note] [Entrypoint]: Database files initialized
wordpress-db-1         | 2023-09-03 22:41:21+00:00 [Note] [Entrypoint]: Starting temporary server
wordpress-db-1         | 2023-09-03 22:41:21+00:00 [Note] [Entrypoint]: Waiting for server startup
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] mysqld (server 10.6.4-MariaDB-1:10.6.4+maria~focal) starting as process 110 ...
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Number of pools: 1
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Using crc32 + pclmulqdq instructions
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] mysqld: O_TMPFILE is not supported on /tmp (disabling future attempts)
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Using Linux native AIO
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Initializing buffer pool, total size = 134217728, chunk size = 134217728
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Completed initialization of buffer pool
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: 128 rollback segments are active.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Creating shared tablespace for temporary tables
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: 10.6.4 started; log sequence number 41361; transaction id 14
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] Plugin 'FEEDBACK' is disabled.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Warning] 'default-authentication-plugin' is MySQL 5.6 / 5.7 compatible option. To be implemented in later versions.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Warning] You need to use --log-bin to make --expire-logs-days or --binlog-expire-logs-seconds work.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Warning] 'user' entry 'root@85287ba5f9c1' ignored in --skip-name-resolve mode.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Warning] 'proxies_priv' entry '@% root@85287ba5f9c1' ignored in --skip-name-resolve mode.
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] mysqld: ready for connections.
wordpress-db-1         | Version: '10.6.4-MariaDB-1:10.6.4+maria~focal'  socket: '/run/mysqld/mysqld.sock'  port: 0  mariadb.org binary distribution
wordpress-db-1         | 2023-09-03 22:41:21 0 [Note] InnoDB: Buffer pool(s) load completed at 230903 22:41:21
wordpress-db-1         | 2023-09-03 22:41:22+00:00 [Note] [Entrypoint]: Temporary server started.
wordpress-db-1         | Warning: Unable to load '/usr/share/zoneinfo/leap-seconds.list' as time zone. Skipping it.
wordpress-db-1         | Warning: Unable to load '/usr/share/zoneinfo/leapseconds' as time zone. Skipping it.
wordpress-db-1         | Warning: Unable to load '/usr/share/zoneinfo/tzdata.zi' as time zone. Skipping it.
wordpress-db-1         | 2023-09-03 22:41:24 5 [Warning] 'proxies_priv' entry '@% root@85287ba5f9c1' ignored in --skip-name-resolve mode.
wordpress-db-1         | 2023-09-03 22:41:25+00:00 [Note] [Entrypoint]: Creating database wordpress
wordpress-db-1         | 2023-09-03 22:41:25+00:00 [Note] [Entrypoint]: Creating user wordpress
wordpress-db-1         | 2023-09-03 22:41:25+00:00 [Note] [Entrypoint]: Giving user wordpress access to schema wordpress
wordpress-db-1         | 
wordpress-db-1         | 2023-09-03 22:41:25+00:00 [Note] [Entrypoint]: Stopping temporary server
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] mysqld (initiated by: root[root] @ localhost []): Normal shutdown
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: FTS optimize thread exiting.
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: Starting shutdown...
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: Dumping buffer pool(s) to /var/lib/mysql/ib_buffer_pool
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: Buffer pool(s) dump completed at 230903 22:41:25
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: Removed temporary tablespace data file: "./ibtmp1"
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] InnoDB: Shutdown completed; log sequence number 42335; transaction id 15
wordpress-db-1         | 2023-09-03 22:41:25 0 [Note] mysqld: Shutdown complete
wordpress-db-1         | 
wordpress-db-1         | 2023-09-03 22:41:26+00:00 [Note] [Entrypoint]: Temporary server stopped
wordpress-db-1         | 
wordpress-db-1         | 2023-09-03 22:41:26+00:00 [Note] [Entrypoint]: MariaDB init process done. Ready for start up.
wordpress-db-1         | 
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] mysqld (server 10.6.4-MariaDB-1:10.6.4+maria~focal) starting as process 1 ...
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Number of pools: 1
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Using crc32 + pclmulqdq instructions
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] mysqld: O_TMPFILE is not supported on /tmp (disabling future attempts)
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Using Linux native AIO
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Initializing buffer pool, total size = 134217728, chunk size = 134217728
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Completed initialization of buffer pool
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: 128 rollback segments are active.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Creating shared tablespace for temporary tables
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: 10.6.4 started; log sequence number 42335; transaction id 14
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] Plugin 'FEEDBACK' is disabled.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Warning] 'default-authentication-plugin' is MySQL 5.6 / 5.7 compatible option. To be implemented in later versions.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Warning] You need to use --log-bin to make --expire-logs-days or --binlog-expire-logs-seconds work.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] Server socket created on IP: '0.0.0.0'.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] Server socket created on IP: '::'.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Warning] 'proxies_priv' entry '@% root@85287ba5f9c1' ignored in --skip-name-resolve mode.
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] mysqld: ready for connections.
wordpress-db-1         | Version: '10.6.4-MariaDB-1:10.6.4+maria~focal'  socket: '/run/mysqld/mysqld.sock'  port: 3306  mariadb.org binary distribution
wordpress-db-1         | 2023-09-03 22:41:26 0 [Note] InnoDB: Buffer pool(s) load completed at 230903 22:41:26
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:58 +0000] "GET /wp-admin HTTP/1.1" 301 755 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:58 +0000] "GET /wp-admin/ HTTP/1.1" 302 460 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:58 +0000] "GET /wp-admin/install.php HTTP/1.1" 200 4721 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-includes/css/buttons.min.css?ver=6.3.1 HTTP/1.1" 200 1790 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-includes/css/dashicons.min.css?ver=6.3.1 HTTP/1.1" 200 36069 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-includes/js/jquery/jquery-migrate.min.js?ver=3.4.1 HTTP/1.1" 200 5223 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/css/install.min.css?ver=6.3.1 HTTP/1.1" 200 2163 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/css/forms.min.css?ver=6.3.1 HTTP/1.1" 200 6858 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/css/l10n.min.css?ver=6.3.1 HTTP/1.1" 200 1022 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/js/language-chooser.min.js?ver=6.3.1 HTTP/1.1" 200 623 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-includes/js/jquery/jquery.min.js?ver=3.7.0 HTTP/1.1" 200 30696 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/install.php" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/images/wordpress-logo.svg?ver=20131107 HTTP/1.1" 200 1810 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/css/install.min.css?ver=6.3.1" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 172.18.0.1 - - [03/Sep/2023:22:43:59 +0000] "GET /wp-admin/images/spinner.gif HTTP/1.1" 200 3941 "http://ip172-18-0-19-cjqg074snmng008vs1v0-80.direct.labs.play-with-docker.com/wp-admin/css/install.min.css?ver=6.3.1" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0"
wordpress-wordpress-1  | 127.0.0.1 - - [03/Sep/2023:22:44:05 +0000] "OPTIONS * HTTP/1.0" 200 126 "-" "Apache/2.4.56 (Debian) PHP/8.0.30 (internal dummy connection)"
wordpress-wordpress-1  | 127.0.0.1 - - [03/Sep/2023:22:44:06 +0000] "OPTIONS * HTTP/1.0" 200 126 "-" "Apache/2.4.56 (Debian) PHP/8.0.30 (internal dummy connection)"
^CGracefully stopping... (press Ctrl+C again to force)
Aborting on container exit...
[+] Stopping 2/2
 ✔ Container wordpress-wordpress-1  Stopped                                                                1.3s 
 ✔ Container wordpress-db-1         Stopped                                                                0.5s 
canceled
[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
# Subindo serviços mais complexos (Flask + redis)

[node1] (local) root@192.168.0.13 ~/minha-imagem/wordpress
$ cd ..
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mkdir compose_flask && cd compose_flask
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ vim app.py
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ cat app.py 
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
  retries = 5
  while True:
    try:
      return cache.incr('hits')
    except redis.exceptions.ConnectionError as exc:
      if retries == 0:
        raise exc
      retries -= 1
      time.sleep(0.5)

@app.route('/')
def hello():
  count = get_hit_count()
  return 'Hello World! I have been seen {} times.\n'.format(count)
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ vim requirements.txt
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ cat requirements.txt 
flask
redis
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ vim Dockerfile
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ vim docker-compose.yml
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ cat docker-compose.yml 
services:
  web:
    build: .
    ports:
    - "8000:5000"
  redis:
    image: "redis:alpine"
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ docker-compose up
[+] Running 7/7
 ✔ redis 6 layers [⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                          1.6s 
   ✔ 7264a8db6415 Pull complete                                                                            0.4s 
   ✔ a28817da73be Pull complete                                                                            0.4s 
   ✔ 536ccaebaffb Pull complete                                                                            0.9s 
   ✔ f54d1871dea6 Pull complete                                                                            1.3s 
   ✔ 4d190b4b6472 Pull complete                                                                            1.3s 
   ✔ 33fcc95c965f Pull complete                                                                            1.3s 
[+] Building 12.3s (13/13) FINISHED                                                                             
 => [web internal] load build definition from Dockerfile                                                   0.0s
 => => transferring dockerfile: 320B                                                                       0.0s
 => [web internal] load .dockerignore                                                                      0.0s
 => => transferring context: 2B                                                                            0.0s
 => [web] resolve image config for docker.io/docker/dockerfile:1                                           0.3s
 => [web] docker-image://docker.io/docker/dockerfile:1@sha256:ac85f380a63b13dfcefa89046420e1781752bab2021  0.4s
 => => resolve docker.io/docker/dockerfile:1@sha256:ac85f380a63b13dfcefa89046420e1781752bab202122f8f50032  0.0s
 => => sha256:657fcc512c7369f4cb3d94ea329150f8daf626bc838b1a1e81f1834c73ecc77e 482B / 482B                 0.0s
 => => sha256:a17ee7fff8f5e97b974f5b48f51647d2cf28d543f2aa6c11aaa0ea431b44bb89 1.27kB / 1.27kB             0.0s
 => => sha256:9d9c93f4b00be908ab694a4df732570bced3b8a96b7515d70ff93402179ad232 11.80MB / 11.80MB           0.1s
 => => sha256:ac85f380a63b13dfcefa89046420e1781752bab202122f8f50032edf31be0021 8.40kB / 8.40kB             0.0s
 => => extracting sha256:9d9c93f4b00be908ab694a4df732570bced3b8a96b7515d70ff93402179ad232                  0.3s
 => [web internal] load metadata for docker.io/library/python:3.7-alpine                                   0.3s
 => [web 1/6] FROM docker.io/library/python:3.7-alpine@sha256:9d9b05fc8acdc85a9fc0da1da11a8e90f76b88bd36f  2.3s
 => => resolve docker.io/library/python:3.7-alpine@sha256:9d9b05fc8acdc85a9fc0da1da11a8e90f76b88bd36fabb8  0.0s
 => => extracting sha256:66e1d5e70e420aa86a23bd8b4eebf2a6eb60b4aff9ee8a6ca52e27f51f57b1be                  0.4s
 => => sha256:1b091ebaa5db12afd53b9c1fd5cd9fe95751ffb789c2ce5e530e6a66f535d1f8 6.87kB / 6.87kB             0.0s
 => => sha256:66e1d5e70e420aa86a23bd8b4eebf2a6eb60b4aff9ee8a6ca52e27f51f57b1be 622.31kB / 622.31kB         0.0s
 => => sha256:66bedaca5a2f87efebbba7f2fc4fa9d6cea1ab46679fc6292458dc4d43722f25 10.94MB / 10.94MB           0.2s
 => => sha256:f26771d857d7bfe2ecd624f357e8391f27f30ec6960d4c1240babf76a8391466 243B / 243B                 0.1s
 => => sha256:9125851493e0d48767e4aff3b4a1cada72207ab01af779634df29f30ac552056 2.85MB / 2.85MB             0.1s
 => => sha256:9d9b05fc8acdc85a9fc0da1da11a8e90f76b88bd36fabb8f57c4c7ef027fbcc9 1.65kB / 1.65kB             0.0s
 => => sha256:0c844a18f7b7b7177cdbe35d5ec934a7801bab3470817321902a6f57c7f67c24 1.37kB / 1.37kB             0.0s
 => => extracting sha256:66bedaca5a2f87efebbba7f2fc4fa9d6cea1ab46679fc6292458dc4d43722f25                  0.8s
 => => extracting sha256:f26771d857d7bfe2ecd624f357e8391f27f30ec6960d4c1240babf76a8391466                  0.0s
 => => extracting sha256:9125851493e0d48767e4aff3b4a1cada72207ab01af779634df29f30ac552056                  0.5s
 => [web internal] load build context                                                                      0.0s
 => => transferring context: 1.02kB                                                                        0.0s
 => [web 2/6] WORKDIR /code                                                                                0.0s
 => [web 3/6] RUN apk add --no-cache gcc musl-dev linux-headers                                            2.5s
 => [web 4/6] COPY requirements.txt requirements.txt                                                       0.1s
 => [web 5/6] RUN pip install -r requirements.txt                                                          4.7s
 => [web 6/6] COPY . .                                                                                     0.0s
 => [web] exporting to image                                                                               1.1s
 => => exporting layers                                                                                    1.1s
 => => writing image sha256:759f5f8289d59b2b85f406fb804e8b1a9a3e5fac80fa22d9b66e1ef0d8912a9a               0.0s
 => => naming to docker.io/library/compose_flask-web                                                       0.0s
[+] Running 3/3
 ✔ Network compose_flask_default    Created                                                                0.1s 
 ✔ Container compose_flask-redis-1  Created                                                                0.1s 
 ✔ Container compose_flask-web-1    Created                                                                0.1s 
Attaching to compose_flask-redis-1, compose_flask-web-1
compose_flask-redis-1  | 1:C 03 Sep 2023 23:02:22.768 # WARNING Memory overcommit must be enabled! Without it, a background save or replication may fail under low memory condition. Being disabled, it can also cause failures without low memory condition, see https://github.com/jemalloc/jemalloc/issues/1328. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
compose_flask-redis-1  | 1:C 03 Sep 2023 23:02:22.768 * oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
compose_flask-redis-1  | 1:C 03 Sep 2023 23:02:22.768 * Redis version=7.2.0, bits=64, commit=00000000, modified=0, pid=1, just started
compose_flask-redis-1  | 1:C 03 Sep 2023 23:02:22.768 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
compose_flask-redis-1  | 1:M 03 Sep 2023 23:02:22.769 * monotonic clock: POSIX clock_gettime
compose_flask-redis-1  | 1:M 03 Sep 2023 23:02:22.770 * Running mode=standalone, port=6379.
compose_flask-redis-1  | 1:M 03 Sep 2023 23:02:22.770 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
compose_flask-redis-1  | 1:M 03 Sep 2023 23:02:22.771 * Server initialized
compose_flask-redis-1  | 1:M 03 Sep 2023 23:02:22.771 * Ready to accept connections tcp
compose_flask-web-1    |  * Serving Flask app 'app.py'
compose_flask-web-1    |  * Debug mode: off
compose_flask-web-1    | WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
compose_flask-web-1    |  * Running on all addresses (0.0.0.0)
compose_flask-web-1    |  * Running on http://127.0.0.1:5000
compose_flask-web-1    |  * Running on http://172.22.0.2:5000
compose_flask-web-1    | Press CTRL+C to quit
compose_flask-web-1    | 172.18.0.1 - - [03/Sep/2023 23:02:27] "GET / HTTP/1.1" 200 -
compose_flask-web-1    | 172.18.0.1 - - [03/Sep/2023 23:02:27] "GET /favicon.ico HTTP/1.1" 404 -
compose_flask-web-1    | 172.18.0.1 - - [03/Sep/2023 23:02:31] "GET / HTTP/1.1" 200 -
compose_flask-web-1    | 172.18.0.1 - - [03/Sep/2023 23:02:33] "GET / HTTP/1.1" 200 -
compose_flask-web-1    | 172.18.0.1 - - [03/Sep/2023 23:02:36] "GET / HTTP/1.1" 200 -
^CGracefully stopping... (press Ctrl+C again to force)
Aborting on container exit...
[+] Stopping 2/2
 ✔ Container compose_flask-redis-1  Stopped                                                                0.3s 
 ✔ Container compose_flask-web-1    Stopped                                                               10.3s 
canceled
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
```
# Enviando uma Imagem para o Docker Hub
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: antoniokassio@gmail.com
Password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ FROM ubuntu:latest
RUN apt-get update && apt-get install -y curl
CMD ["curl", "https://gitlab.com"]^C
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ vim Dockerfile
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ mkdir imagem-para-dockerhub
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ cd imagem-para-dockerhub/
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask/imagem-para-dockerhub
$ cd ..
[node1] (local) root@192.168.0.13 ~/minha-imagem/compose_flask
$ cd ..
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mkdir ^C
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ mkdir imagem-para-dockerhub
[node1] (local) root@192.168.0.13 ~/minha-imagem
$ cd imagem-para-dockerhub/
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ vim Dockerfile
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$  docker build -t seu-usuario/exemplo-de-imagem:1.0 .
[+] Building 0.1s (6/6) FINISHED                                                                                
 => [internal] load build definition from Dockerfile                                                       0.0s
 => => transferring dockerfile: 137B                                                                       0.0s
 => [internal] load .dockerignore                                                                          0.0s
 => => transferring context: 2B                                                                            0.0s
 => [internal] load metadata for docker.io/library/ubuntu:latest                                           0.0s
 => [1/2] FROM docker.io/library/ubuntu:latest                                                             0.0s
 => CACHED [2/2] RUN apt-get update && apt-get install -y curl                                             0.0s
 => exporting to image                                                                                     0.0s
 => => exporting layers                                                                                    0.0s
 => => writing image sha256:47a5984859f89058ab44fde245356178336352bf4574f8839ff8b63f041c875b               0.0s
 => => naming to docker.io/seu-usuario/exemplo-de-imagem:1.0                                               0.0s
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker push seu-usuario/exemplo-de-imagem:1.0
The push refers to repository [docker.io/seu-usuario/exemplo-de-imagem]
9051e9c7ab8c: Preparing 
dc0585a4b8b7: Preparing 
denied: requested access to the resource is denied
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker push antoniokassio/exemplo-de-imagem:1.0
The push refers to repository [docker.io/antoniokassio/exemplo-de-imagem]
An image does not exist locally with the tag: antoniokassio/exemplo-de-imagem
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker push antoniokassio/curso-devops:1.0
The push refers to repository [docker.io/antoniokassio/curso-devops]
An image does not exist locally with the tag: antoniokassio/curso-devops
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$  docker build -t antoniokassio/exemplo-de-imagem:1.0 .
[+] Building 0.4s (6/6) FINISHED                                                                                
 => [internal] load build definition from Dockerfile                                                       0.1s
 => => transferring dockerfile: 137B                                                                       0.0s
 => [internal] load .dockerignore                                                                          0.3s
 => => transferring context: 2B                                                                            0.0s
 => [internal] load metadata for docker.io/library/ubuntu:latest                                           0.0s
 => [1/2] FROM docker.io/library/ubuntu:latest                                                             0.0s
 => CACHED [2/2] RUN apt-get update && apt-get install -y curl                                             0.0s
 => exporting to image                                                                                     0.0s
 => => exporting layers                                                                                    0.0s
 => => writing image sha256:47a5984859f89058ab44fde245356178336352bf4574f8839ff8b63f041c875b               0.0s
 => => naming to docker.io/antoniokassio/exemplo-de-imagem:1.0                                             0.0s
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker push antoniokassio/exem:1.0
antoniokassio/exemplo-de-imagem      antoniokassio/exemplo-de-imagem:1.0  
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker push antoniokassio/exemplo-de-imagem:1.0
The push refers to repository [docker.io/antoniokassio/exemplo-de-imagem]
9051e9c7ab8c: Pushed 
dc0585a4b8b7: Pushed 
1.0: digest: sha256:67d6c0937b21c65913679bbbfbda35cb1b32f5a5a0a0e734f915b58afe40add9 size: 741
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ 
```
# Puxando e Executando uma Imagem do Docker Hub
``` language:bash
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$  docker pull antoniokassio/exemplo-de-imagem:1.0
1.0: Pulling from antoniokassio/exemplo-de-imagem
Digest: sha256:67d6c0937b21c65913679bbbfbda35cb1b32f5a5a0a0e734f915b58afe40add9
Status: Image is up to date for antoniokassio/exemplo-de-imagem:1.0
docker.io/antoniokassio/exemplo-de-imagem:1.0
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker run seu-usuario/exemplo-de-imagem:1.0
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    91  100    91    0     0    471      0 --:--:-- --:--:-- --:--:--   471
<html><body>You are being <a href="https://about.gitlab.com/">redirected</a>.</body></html>[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ docker images
REPOSITORY                        TAG            IMAGE ID       CREATED             SIZE
compose_flask-web                 latest         759f5f8289d5   22 minutes ago      213MB
antoniokassio/exemplo-de-imagem   1.0            47a5984859f8   About an hour ago   128MB
minha-imagem                      latest         47a5984859f8   About an hour ago   128MB
seu-usuario/exemplo-de-imagem     1.0            47a5984859f8   About an hour ago   128MB
wordpress                         latest         efa186f6484e   5 days ago          666MB
redis                             alpine         3a8d46c63628   2 weeks ago         37.8MB
nginx                             latest         eea7b3dcba7e   2 weeks ago         187MB
ubuntu                            latest         c6b84b685f35   2 weeks ago         77.8MB
postgres                          latest         43677b39c446   2 weeks ago         412MB
hello-world                       latest         9c7a54a9a43c   4 months ago        13.3kB
mariadb                           10.6.4-focal   12e05d5da3c5   22 months ago       409MB
[node1] (local) root@192.168.0.13 ~/minha-imagem/imagem-para-dockerhub
$ 
``` 
